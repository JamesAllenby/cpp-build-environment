# CPP-Build-Environment

This repository creates a build environment for my C++ projects. It is based on Ubuntu 20.04 and installs the latest updates with CMake, pip3, GCC/G++ and Conan.